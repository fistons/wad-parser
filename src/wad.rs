#![allow(unused)]
use std::{
    borrow::Cow,
    error::Error,
    fs::File,
    io::Write,
    ops::Index,
    path::{Path, PathBuf},
};

/// Size of a WAD headers, in bytes
const WAD_HEADER_SIZE: usize = 12;

/// Size of a directory entry, in bytes
const DIRECTORY_ENTRY_SIZE: usize = 16;

/// Size of a PLAYPAL, in bytes
const PLAYPAL_SIZE: usize = 768;

type Result<K> = std::result::Result<K, Box<dyn Error>>;

#[derive(Debug)]
pub struct Playpal<'a>(Vec<(&'a u8, &'a u8, &'a u8)>);

impl<'a> Playpal<'a> {
    fn extract_playpals<'b>(
        directory: &'b [DirectoryEntry],
        data: &'a [u8],
    ) -> Result<Vec<Playpal<'a>>> {
        let playpals_position = directory.iter().position(|x| x.name == "PLAYPAL").unwrap();

        let lump = &directory[playpals_position];
        let playpals = Playpal::from_bytes(&data[lump.filepos..lump.filepos + lump.size])?;

        Ok(playpals)
    }
    pub fn from_bytes(data: &'a [u8]) -> Result<Vec<Self>> {
        let number_of_palettes = data.len() / PLAYPAL_SIZE;
        let mut all_palettes = Vec::with_capacity(number_of_palettes);
        for palette in 0..number_of_palettes {
            let mut playpal = vec![];
            for i in (0..768).step_by(3) {
                let i = i + palette * PLAYPAL_SIZE;

                let r = &data[i];
                let g = &data[i + 1];
                let b = &data[i + 2];
                playpal.push((r, g, b))
            }
            all_palettes.push(Playpal(playpal));
        }
        Ok(all_palettes)
    }

    pub fn write(&self, output: &PathBuf) -> Result<()> {
        let mut buffer = vec![];
        buffer.extend_from_slice(b"P6\n16 16\n255\n");
        for (r, g, b) in &self.0 {
            buffer.extend_from_slice(&[**r, **g, **b]);
        }

        let mut output_file = File::create(output)?;
        output_file.write_all(&buffer)?;
        Ok(())
    }
}

#[derive(Debug)]
pub struct Wad<'a> {
    pub header: WadHeader<'a>,
    pub directory: Vec<DirectoryEntry<'a>>,
    pub playpals: Vec<Playpal<'a>>,
    // wall_patch: HashMap<String, Patch>,
    data: &'a [u8],
}

impl<'a> Wad<'a> {
    pub fn from_bytes(file: &'a [u8]) -> Result<Self> {
        let header = WadHeader::from_bytes(&file[0..WAD_HEADER_SIZE])?;
        let directory_range =
            header.infotableofs..header.infotableofs + header.numlumps * DIRECTORY_ENTRY_SIZE;
        let directory = Self::parse_directory(&header, &file[directory_range])?;

        let playpals = Playpal::extract_playpals(&directory, file)?;

        Ok(Wad {
            header,
            directory,
            playpals,
            data: file,
        })
    }

    fn parse_directory<'b>(
        header: &'b WadHeader,
        data: &'a [u8],
    ) -> Result<Vec<DirectoryEntry<'a>>> {
        let mut directory_entries = vec![];
        for i in 0..header.numlumps {
            let i = i * 16;

            let position: [u8; 4] = data[i..i + 4].try_into()?;
            let size: [u8; 4] = data[i + 4..i + 8].try_into()?;

            let position = u32::from_le_bytes(position);
            let size = u32::from_le_bytes(size);

            let name = &data[i + 8..i + 16];
            let termination_position = name.iter().position(|x| *x == b'\0').unwrap_or(name.len());
            let name = String::from_utf8_lossy(&name[0..termination_position]);

            directory_entries.push(DirectoryEntry {
                filepos: position as usize,
                name,
                size: size as usize,
            });
        }
        Ok(directory_entries)
    }
}

#[derive(Debug)]
pub struct DirectoryEntry<'a> {
    /// Lump name
    name: Cow<'a, str>,
    size: usize,
    filepos: usize,
}

#[derive(Debug)]
pub struct WadHeader<'a> {
    pub identification: Cow<'a, str>,
    pub numlumps: usize,
    pub infotableofs: usize,
}

impl<'a> WadHeader<'a> {
    pub fn from_bytes(data: &'a [u8]) -> Result<Self> {
        let wad_type: [u8; 4] = data[0..4].try_into()?;
        let entries_number: [u8; 4] = data[4..8].try_into()?;
        let directory_location: [u8; 4] = data[8..12].try_into()?;

        let wad_type = String::from_utf8_lossy(&data[0..4]);
        let entries_number = u32::from_le_bytes(entries_number);
        let directory_location = u32::from_le_bytes(directory_location);

        Ok(WadHeader {
            identification: wad_type,
            numlumps: entries_number as usize,
            infotableofs: directory_location as usize,
        })
    }
}

pub mod patch {
    use super::Playpal;
    use super::Result;
    use std::path::Path;
    use std::path::PathBuf;

    #[derive(Debug)]
    pub struct Patch<'a> {
        header: PatchHeader,
        name: String,
        data: Vec<PatchPost<'a>>,
    }

    #[derive(Debug)]
    pub struct PatchHeader {
        width: u16,
        height: u16,
        left_offset: u16,
        top_offset: u16,
    }

    #[derive(Debug)]
    pub struct PatchPost<'a> {
        top_delta: u8,
        lenght: u8,
        _pre_padding: u8,
        data: &'a [u8],
        _post_padding: u8,
    }

    impl<'a> Patch<'a> {
        pub fn from_bytes(data: &[u8], offset: usize) -> Result<Self> {
            todo!()
        }

        pub fn export_patch(&self, output_file: &Path, playpal: &Playpal) -> Result<()> {
            todo!()
        }
    }

    impl<'a> PatchPost<'a> {}

    impl PatchHeader {}
}
