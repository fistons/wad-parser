#![allow(unused)]
use clap::Parser;

// use memmap::MmapOptions;
use regex::Regex;

use std::fs;
use std::io::{Read, Write};

use std::path::Path;
use std::{error::Error, fs::File, path::PathBuf};

use crate::wad::Wad;

type Result<K> = std::result::Result<K, Box<dyn Error>>;
mod wad;

/// Wad parser. Or somehting like this
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Wad file to parse
    #[arg(short, long)]
    wad: PathBuf,
}

trait Lump {}

#[derive(Debug)]
enum ThingType {
    Player1Start,
    Player2Start,
    Player3Start,
    Player4Start,

    Imp,
    ZombieMan,

    Unknown(u16),
}

impl ThingType {
    fn from(type_id: u16) -> Self {
        match type_id {
            1 => ThingType::Player1Start,
            2 => ThingType::Player2Start,
            3 => ThingType::Player3Start,
            4 => ThingType::Player4Start,
            3001 => ThingType::Imp,
            3004 => ThingType::ZombieMan,
            id => ThingType::Unknown(id),
        }
    }
}

#[derive(Debug)]
struct Header {
    identification: String,
    numlumps: u32,
    infotableofs: u32,
}

#[derive(Debug)]
struct FileLump {
    name: String,
    size: u32,
    filepos: u32,
}

#[derive(Debug)]
struct ThingLump {
    x: u16,
    y: u16,
    angle: u16,
    thing_type: ThingType,
    flags: u16,
}
impl Lump for ThingLump {}

#[derive(Debug)]
struct VertexLump {
    x: u16,
    y: u16,
}
impl Lump for VertexLump {}

#[derive(Debug)]
struct Linedef {
    start: u16,
    end: u16,
}

#[derive(Debug)]
struct PatchHeader {
    width: u16,
    height: u16,
    left_offset: u16,
    top_offset: u16,
    column_offsets: Vec<i32>,
}

enum Namespace {
    Root,
    _Map(String),
    Flats,
    Sprites,
    Patches,
}

fn main() -> Result<()> {
    let args = Args::parse();

    println!("Opening {:?}", args.wad);

    let mut file = std::fs::File::open(&args.wad)?;
    let mut buffer = vec![];
    let file = file.read_to_end(&mut buffer)?;
    // let file = unsafe { MmapOptions::new().map(&buffer[..])? };

    let wad = Wad::from_bytes(&buffer)?;

    println!("{:?}", wad.header);
    println!("{:?}", wad.directory.len());
    println!("{}", wad.header.numlumps);
    fs::create_dir_all("playpals")?;

    for (i, playpal) in wad.playpals.iter().enumerate() {
        playpal.write(&Path::new("playpals").join(&format!("{i}.ppm")))?;
    }
    // wad.extract_playpal(Path::new("playpals"))?;
    // let header = parse_header(&file)?;
    // println!("Header {header:?}");

    // let mut playpals = None;
    // let mut namespace = Namespace::Root;
    // let start_patches = Regex::new(r"\bP\d_START\b").unwrap();
    // let end_section = Regex::new(r"\b\w+_END\b").unwrap();
    // for i in 0..header.numlumps {
    //     let lump = parse_entry(&file, (header.infotableofs + (i * 16)) as usize)?;
    //     println!("{lump:?}");
    //     match lump.name.as_ref() {
    //         "PLAYPAL" => {
    //             playpals = Some(parse_playpal(
    //                 &file,
    //                 lump.filepos as usize,
    //                 lump.size as usize,
    //             )?);
    //         }
    //         "F_START" | "FF_START" => namespace = Namespace::Flats,
    //         x if end_section.is_match(x) => namespace = Namespace::Root,
    //         x if start_patches.is_match(x) => namespace = Namespace::Patches,
    //         "S_START" | "SS_START" => namespace = Namespace::Sprites,
    //         x if x.starts_with("TEXTURE") => {
    //             extract_textures(&file, lump.filepos as usize, lump.size as usize)?
    //         }
    //         n => match namespace {
    //             Namespace::Flats => {
    //                 extract_flats(
    //                     &file,
    //                     lump.filepos as usize,
    //                     lump.size as usize,
    //                     n,
    //                     &playpals,
    //                     0,
    //                 )?;
    //             }
    //             Namespace::Sprites => {
    //                 write_patch(
    //                     &file,
    //                     lump.filepos as usize,
    //                     lump.size as usize,
    //                     n,
    //                     "sprites",
    //                     &playpals,
    //                 )?;
    //             }
    //             Namespace::Patches => {
    //                 write_patch(
    //                     &file,
    //                     lump.filepos as usize,
    //                     lump.size as usize,
    //                     n,
    //                     "patches",
    //                     &playpals,
    //                 )?;
    //             }
    //             _ => (),
    //         }, // println!("Unknown entry: {n}"),
    //     }
    // }

    Ok(())
}

fn parse_header(file: &[u8]) -> Result<Header> {
    let wad_type: [u8; 4] = file[0..4].try_into()?;
    let entries_number: [u8; 4] = file[4..8].try_into()?;
    let directory_location: [u8; 4] = file[8..12].try_into()?;

    let wad_type = String::from_utf8_lossy(&wad_type).to_string();
    let entries_number = u32::from_le_bytes(entries_number);
    let directory_location = u32::from_le_bytes(directory_location);

    Ok(Header {
        identification: wad_type,
        numlumps: entries_number,
        infotableofs: directory_location,
    })
}

fn parse_entry(file: &[u8], offset: usize) -> Result<FileLump> {
    let position: [u8; 4] = file[offset..offset + 4].try_into()?;
    let size: [u8; 4] = file[offset + 4..offset + 8].try_into()?;
    let name: [u8; 8] = file[offset + 8..offset + 16].try_into()?;

    let position = u32::from_le_bytes(position);
    let size = u32::from_le_bytes(size);
    let name = String::from_utf8_lossy(&name)
        .to_string()
        .trim_matches(char::from(0))
        .to_owned();

    Ok(FileLump {
        filepos: position,
        name,
        size,
    })
}

fn _parse_things(file: &[u8], offset: usize, size: usize) -> Result<Vec<ThingLump>> {
    let number_of_things = size / 10;
    let mut things = Vec::with_capacity(number_of_things);

    for i in 0..number_of_things {
        let offset = offset + (i * 10);
        let x: [u8; 2] = file[offset..offset + 2].try_into()?;
        let y: [u8; 2] = file[offset + 2..offset + 4].try_into()?;
        let angle: [u8; 2] = file[offset + 4..offset + 6].try_into()?;
        let thing_type: [u8; 2] = file[offset + 6..offset + 8].try_into()?;
        let flags: [u8; 2] = file[offset + 8..offset + 10].try_into()?;

        let x = u16::from_le_bytes(x);
        let y = u16::from_le_bytes(y);
        let angle = u16::from_le_bytes(angle);
        let thing_type = u16::from_le_bytes(thing_type);
        let flags = u16::from_le_bytes(flags);

        things.push(ThingLump {
            x,
            y,
            angle,
            thing_type: ThingType::from(thing_type),
            flags,
        });
    }

    Ok(things)
}

fn _parse_vertexes(file: &[u8], offset: usize, size: usize) -> Result<Vec<VertexLump>> {
    let number_of_lump = size / 4;
    let mut vertexes = Vec::with_capacity(number_of_lump);

    for i in 0..number_of_lump {
        let offset = offset + (i * 4);
        let x: [u8; 2] = file[offset..offset + 2].try_into()?;
        let y: [u8; 2] = file[offset + 2..offset + 4].try_into()?;

        let x = u16::from_le_bytes(x);
        let y = u16::from_le_bytes(y);

        vertexes.push(VertexLump { x, y });
    }

    Ok(vertexes)
}

fn _parse_linedef(file: &[u8], offset: usize, size: usize) -> Result<Vec<Linedef>> {
    let number_of_lump = size / 14;
    let mut linedefs = Vec::with_capacity(number_of_lump);

    for i in 0..number_of_lump {
        let offset = offset + (i * 14);
        let start: [u8; 2] = file[offset..offset + 2].try_into()?;
        let end: [u8; 2] = file[offset + 2..offset + 4].try_into()?;

        let start = u16::from_le_bytes(start);
        let end = u16::from_le_bytes(end);

        linedefs.push(Linedef { start, end });
    }

    Ok(linedefs)
}

fn extract_flats(
    file: &[u8],
    offset: usize,
    _size: usize,
    name: &str,
    playpals: &Option<Vec<Playpal>>,
    playpal_number: usize,
) -> Result<()> {
    let Some(playpals) = playpals else {
        return Err("Could not extract flat: Playpals are empty".into());
    };
    fs::create_dir_all("flats")?;
    let mut output = File::create(format!("flats/{playpal_number}_{name}.ppm"))?;
    let mut buffer = vec![];
    buffer.extend_from_slice(b"P6\n64 64\n255\n");
    for y in 0..64 {
        for x in 0..64 {
            let index_pixel = file[offset + (y * 64) + x];
            let (r, g, b) = playpals[playpal_number][index_pixel as usize];
            buffer.extend_from_slice(&[r, g, b]);
        }
    }
    output.write_all(&buffer)?;
    Ok(())
}

fn parse_playpal(file: &[u8], offset: usize, size: usize) -> Result<Vec<Playpal>> {
    let number_of_palette = size / 768;
    let mut all_playpals = vec![];
    fs::create_dir_all("playpals")?;
    for palette_index in 0..number_of_palette {
        let mut playpal = vec![];
        let offset = offset + (palette_index * 768);
        let palette = &file[offset..offset + 768];
        let mut output = File::create(format!("playpals/PLAYPAL_{palette_index}.ppm"))?;
        let mut buffer = vec![];
        buffer.extend_from_slice(b"P6\n16 16\n255\n");
        for i in (0..768).step_by(3) {
            let r = palette[i];
            let g = palette[i + 1];
            let b = palette[i + 2];
            buffer.extend_from_slice(&[r, g, b]);
            playpal.push((r, g, b))
        }
        all_playpals.push(playpal);
        output.write_all(&buffer)?;
    }

    Ok(all_playpals)
}

fn write_patch(
    file: &[u8],
    offset: usize,
    _size: usize,
    name: &str,
    category: &str,
    playpals: &Option<Vec<Playpal>>,
) -> Result<()> {
    let Some(playpals) = playpals else {
        return Err("Could not write patches: Playpals are empty".into());
    };

    let (width, height, image) = extract_patch(file, offset)?;

    let sprite_name = &name[0..4];
    fs::create_dir_all(format!("patches/{category}/{sprite_name}"))?;
    let mut output = File::create(format!("patches/{category}/{sprite_name}/{name}.ppm"))?;

    let mut buffer = vec![];
    buffer.extend_from_slice(format!("P6\n{width} {height}\n255\n").as_bytes());
    for x in image.iter() {
        let (r, g, b) = playpals[0][*x as usize];
        buffer.extend_from_slice(&[r, g, b]);
    }
    output.write_all(&buffer)?;
    Ok(())
}

fn extract_patch(file: &[u8], offset: usize) -> Result<(u16, u16, Vec<u8>)> {
    let width: [u8; 2] = file[offset..offset + 2].try_into()?;
    let height: [u8; 2] = file[offset + 2..offset + 4].try_into()?;
    let left_offset: [u8; 2] = file[offset + 4..offset + 6].try_into()?;
    let top_offset: [u8; 2] = file[offset + 6..offset + 8].try_into()?;
    let mut column_offsets = vec![];

    let height = u16::from_le_bytes(height);
    let width = u16::from_le_bytes(width);
    let left_offset = u16::from_le_bytes(left_offset);
    let top_offset = u16::from_le_bytes(top_offset);
    for i in 0..width as usize {
        let offset = offset + 8 + (i * 4);
        let col_off: [u8; 4] = file[offset..offset + 4].try_into()?;
        let col_off = i32::from_le_bytes(col_off);
        column_offsets.push(col_off);
    }

    let header = PatchHeader {
        width,
        height,
        left_offset,
        top_offset,
        column_offsets,
    };

    let mut image = vec![255u8; (height * width) as usize];
    for col_index in 0..width {
        let mut post_index = 0;

        loop {
            let post_offset =
                header.column_offsets[col_index as usize] as usize + offset + post_index;

            let top_delta = file[post_offset];
            if top_delta == 255 {
                break;
            }
            let length = file[post_offset + 1];
            // let _unused = file[post_offset + 2]; // Unused padding
            let data = &file[post_offset + 3..post_offset + 3 + length as usize];
            // let _unused = file[post_offset + 3 + length as usize]; // Unsused padding

            for (i, y) in (top_delta..top_delta + length).enumerate() {
                image[y as usize * width as usize + col_index as usize] = data[i];
            }
            post_index += 4 + length as usize;
        }
    }
    Ok((width, height, image))
}

fn extract_textures(file: &[u8], offset: usize, size: usize) -> Result<()> {
    let num_textures: [u8; 4] = file[offset..offset + 4].try_into()?;
    let num_textures = u32::from_le_bytes(num_textures);
    // println!("Number of textures {num_textures}");
    for index in 0..num_textures as usize {
        let texture_offet: [u8; 4] =
            file[offset + 4 + (index * 4)..offset + 8 + (index * 4)].try_into()?;
        let texture_offset = u32::from_le_bytes(texture_offet) as usize;
        // println!("Index of texture {index}: {texture_offset}");

        extract_single_texture(file, texture_offset + offset)?;
    }

    Ok(())
}

fn extract_single_texture(file: &[u8], texture_index: usize) -> Result<()> {
    let texture_name = String::from_utf8_lossy(&file[texture_index..texture_index + 8]);
    let _masked = &file[texture_index + 8..texture_index + 12];
    let width: [u8; 2] = file[texture_index + 12..texture_index + 14].try_into()?;
    let height: [u8; 2] = file[texture_index + 14..texture_index + 16].try_into()?;
    let _column_directory = &file[texture_index + 16..texture_index + 20];
    let patch_number: [u8; 2] = file[texture_index + 20..texture_index + 22].try_into()?;

    let width = u16::from_le_bytes(width);
    let height = u16::from_le_bytes(height);
    let patch_number = u16::from_le_bytes(patch_number);
    // println!("Texture {texture_name}  {width}x{height} {patch_number} patchs");

    Ok(())
}

type Playpal = Vec<(u8, u8, u8)>;
